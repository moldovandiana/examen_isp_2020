import javax.swing.*;
import java.awt.*;
import java.io.*;
public class ex2 extends JFrame {
    JTextField nume_fisier;
    JTextField fisierscriere;
    ex2() {
        this.setSize(400, 500);
        this.setLayout(new GridLayout(3, 1));
        this.nume_fisier = new JTextField();
        nume_fisier.setSize(80, 20);
        this.add(nume_fisier);
        this.fisierscriere=new JTextField();
        fisierscriere.setSize(80,20);
        this.add(fisierscriere);
        JButton button=new JButton("Write");
        button.setSize(100,20);
        button.addActionListener(e->{
            String path=nume_fisier.getText();
            File file=new File(path);
            try {
                BufferedReader bufferedReader= new BufferedReader(new FileReader(file));
                String scrierefisier=bufferedReader.readLine();
                fisierscriere.setText(scrierefisier);
                bufferedReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        this.add(button);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        new ex2();
    }
}